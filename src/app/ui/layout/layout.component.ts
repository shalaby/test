import { Component, OnInit } from '@angular/core';
import {NavigationStart, Router} from '@angular/router';

@Component({
  selector: 'app-layout',
  template: `
    <div class="main-container">
      <app-header *ngIf="url != '/login'"></app-header>
      <app-main>
        <ng-content></ng-content>
      </app-main>
    </div>
  `,
  styles: []
})
export class LayoutComponent implements OnInit {
  url: string;

  constructor(private router: Router) {
    this.router.events.subscribe((e: NavigationStart) => {
      this.url = e.url;
    })
  }

  ngOnInit() {
  }

}
