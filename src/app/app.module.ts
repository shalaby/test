import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DaysModule} from './days/days.module';
import {OrdersModule} from './orders/orders.module';
import {ProductsModule} from './products/products.module';
import {SharedModule} from './shared/shared.module';
import {UiModule} from './ui/ui.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    UiModule,
    ProductsModule,
    OrdersModule,
    DaysModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
