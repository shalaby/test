import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {ProductAddComponent} from './product-add/product-add.component';
import {ProductEditComponent} from './product-edit/product-edit.component';
import {ProductListComponent} from './product-list/product-list.component';
import {ProductsRouting} from './shared/product.routing';
import {ProductService} from './shared/product.service';

@NgModule({
  imports: [
    SharedModule,
    ProductsRouting
  ],
  declarations: [
      ProductAddComponent,
      ProductEditComponent,
      ProductListComponent
  ],
  providers: [ProductService]
})
export class ProductsModule {
}
