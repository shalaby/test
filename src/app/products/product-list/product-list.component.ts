import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Product} from '../shared/product.model';
import {ProductService} from '../shared/product.service';

@Component({
  selector   : 'app-product-list',
  templateUrl: './product-list.component.html',
  styles     : []
})
export class ProductListComponent implements OnInit {

  @Output() editChange = new EventEmitter();
  showAddProduct = false;
  showEditProduct = false;
  product: Product;
  products: Product[];
  rows = 50;
  notifications: any;
  loading = false;
  index: number;

  constructor(private productService: ProductService) {
  }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.productService.getProducts()
        .subscribe(products => this.products = products);
  }

  edit(product, index) {
    this.product = product;
    this.index = index;
    this.showEditProduct = true;
  }

  add(newProduct: Product) {
    if (newProduct) {
      this.products = [newProduct, ...this.products];
    }
  }

  update(updatedProduct: Product, index: number) {
    if (updatedProduct) {
      this.products = [...this.products.slice(0, index), updatedProduct, ...this.products.slice(index + 1)];
    }
  }

  delete(id: number, index: number) {
    this.productService.deleteProduct(id)
        .subscribe(
            res => {
              this.products = [...this.products.slice(0, index), ...this.products.slice(index + 1)];
            },
            error => this.displayErrorMessage(error)
        );
  }

  closeDialog(event) {
    switch (event.type) {
      case 'add':
        this.showAddProduct = false;
        this.add(event.data);
        break;
      case 'edit':
        this.showEditProduct = false;
        this.update(event.data, this.index);
        break;
      case 'error':
        this.displayErrorMessage(event.data);
        break;
      default:
        this.showAddProduct = false;
        this.showEditProduct = false;
        break;
    }
  }

  displayErrorMessage(error) {
    this.notifications = error;
  }
}
