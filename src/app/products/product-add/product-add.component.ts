import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Product} from '../shared/product.model';
import {ProductService} from '../shared/product.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styles: ['']
})
export class ProductAddComponent implements OnInit {

  product: Product;
  @Input() show = false;
  @Output() hide = new EventEmitter();
  productForm: FormGroup;

  constructor(private productService: ProductService,
              private formBuilder: FormBuilder) {
    this.productForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      price: ['', [Validators.required]]
    });
  }

  ngOnInit() {
  }

  save(productForm) {
    const product = productForm.value;
    this.productService.addProduct(product)
        .subscribe(res => {
              product.id = res.insertId;
              this.closeDialog({type: 'add', data: product});
            },
            err => {
              this.closeDialog({type: 'error', data: err});
            });
  }

  closeDialog(res) {
    this.productForm.reset();
    this.hide.emit(res);
  }
}
