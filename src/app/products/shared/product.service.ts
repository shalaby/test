import {Injectable} from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/share';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Product} from './product.model';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';

@Injectable()
export class ProductService {
  url: string = environment.serverUrl + 'products/';
  headers: HttpHeaders;
  authToken: any;

  constructor(private http: HttpClient, private router: Router) {
    this.authToken = localStorage.getItem('auth_token');

    this.headers = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', this.authToken);
  }

  getProducts(): Observable<any> {
    return this.http.get(this.url)
        .share()
        .catch(this.handleError.bind(this));
  }

  addProduct(product: Product): Observable<any> {

    return this.http.post(this.url, product)
        .catch(this.handleError.bind(this));
  }

  updateProduct(product: Product): Observable<any> {

    return this.http.put(this.url + product.id, product)
        .catch(this.handleError.bind(this));
  }

  deleteProduct(id: number): Observable<any> {
    return this.http.delete(this.url + id)
        .catch(this.handleError.bind(this));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 403) {
      return this.router.navigate(['/logout']);
    }
    return ErrorObservable.create(error);
  }

}
