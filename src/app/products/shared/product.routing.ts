import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProductListComponent} from '../product-list/product-list.component';

const productsRoutes: Routes = [
  {
    path    : 'products',
    children: [
      {path: '', component: ProductListComponent}
    ]
  }
];

export const ProductsRouting: ModuleWithProviders = RouterModule.forChild(productsRoutes);
