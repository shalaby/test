import {Component, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {Product} from '../shared/product.model';
import {ProductService} from '../shared/product.service';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';

@Component({
  selector   : 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styles  : ['']
})
export class ProductEditComponent implements OnChanges {

  @Input('product') product: Product;
  @Input() show = false;
  @Output() hide = new EventEmitter();
  productForm: FormGroup;

  constructor(private productService: ProductService,
              private formBuilder: FormBuilder) {
    this.productForm = this.formBuilder.group({
      name      : ['', [Validators.required, Validators.minLength(4)]],
      price     : ['', Validators.required],
    });
  }

  async save(productForm) {
    const product = productForm.value;
    product.id = this.product.id;

    this.productService.updateProduct(product)
        .subscribe(res => {
              this.closeDialog({type: 'edit', data: product});
            },
            err => {
              this.closeDialog({type: 'error', data: err});
            });
  }

  closeDialog(result) {
    // this.productForm.reset();
    this.hide.emit(result);
  }

  ngOnChanges(change) {
    if (change.product) {
      if (change.product.currentValue) {
        this.productForm.patchValue({
          name      : change.product.currentValue.name,
          price     : change.product.currentValue.price
        });
      }
    }
  }
}
