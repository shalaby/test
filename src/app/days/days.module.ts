import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {DayAddComponent} from './day-add/day-add.component';
import {DayEditComponent} from './day-edit/day-edit.component';
import {DayListComponent} from './day-list/day-list.component';
import {DaysRouting} from './shared/day.routing';
import {DayService} from './shared/day.service';

@NgModule({
  imports: [
    SharedModule,
    DaysRouting
  ],
  declarations: [
      DayAddComponent,
      DayEditComponent,
      DayListComponent
  ],
  providers: [DayService]
})
export class DaysModule {
}
