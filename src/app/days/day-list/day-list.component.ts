import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Day} from '../shared/day.model';
import {DayService} from '../shared/day.service';

@Component({
  selector   : 'app-day-list',
  templateUrl: './day-list.component.html',
  styles     : []
})
export class DayListComponent implements OnInit {

  @Output() editChange = new EventEmitter();
  showAddDay = false;
  showEditDay = false;
  day: Day;
  days: Day[];
  rows = 50;
  notifications: any;
  loading = false;
  index: number;

  constructor(private dayService: DayService) {
  }

  ngOnInit() {
    this.getDays();
  }

  getDays() {
    this.dayService.getDays()
        .subscribe(days => this.days = days);
  }

  edit(day, index) {
    this.day = day;
    this.index = index;
    this.showEditDay = true;
  }

  add(newDay: Day) {
    if (newDay) {
      this.days = [newDay, ...this.days];
    }
  }

  update(updatedDay: Day, index: number) {
    if (updatedDay) {
      this.days = [...this.days.slice(0, index), updatedDay, ...this.days.slice(index + 1)];
    }
  }

  delete(id: number, index: number) {
    this.dayService.deleteDay(id)
        .subscribe(
            res => {
              this.days = [...this.days.slice(0, index), ...this.days.slice(index + 1)];
            },
            error => this.displayErrorMessage(error)
        );
  }

  closeDialog(event) {
    switch (event.type) {
      case 'add':
        this.showAddDay = false;
        this.add(event.data);
        break;
      case 'edit':
        this.showEditDay = false;
        this.update(event.data, this.index);
        break;
      case 'error':
        this.displayErrorMessage(event.data);
        break;
      default:
        this.showAddDay = false;
        this.showEditDay = false;
        break;
    }
  }

  displayErrorMessage(error) {
    this.notifications = error;
  }
}
