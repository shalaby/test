import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Day} from '../shared/day.model';
import {DayService} from '../shared/day.service';

@Component({
  selector: 'app-day-add',
  templateUrl: './day-add.component.html',
  styles: ['']
})
export class DayAddComponent implements OnInit {

  day: Day;
  @Input() show = false;
  @Output() hide = new EventEmitter();
  dayForm: FormGroup;

  constructor(private dayService: DayService,
              private formBuilder: FormBuilder) {
    this.dayForm = this.formBuilder.group({
      day: ['', [Validators.required, Validators.minLength(4)]],
      from: [''],
      to: ['']
    });
  }

  ngOnInit() {
  }

  save(dayForm) {
    const day = dayForm.value;
    this.dayService.addDay(day)
        .subscribe(res => {
              day.id = res.insertId;
              this.closeDialog({type: 'add', data: day});
            },
            err => {
              this.closeDialog({type: 'error', data: err});
            });
  }

  closeDialog(res) {
    this.dayForm.reset();
    this.hide.emit(res);
  }
}
