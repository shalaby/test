import {Component, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {Day} from '../shared/day.model';
import {DayService} from '../shared/day.service';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';

@Component({
  selector   : 'app-day-edit',
  templateUrl: './day-edit.component.html',
  styles  : ['']
})
export class DayEditComponent implements OnChanges {

  @Input('day') day: Day;
  @Input() show = false;
  @Output() hide = new EventEmitter();
  dayForm: FormGroup;

  constructor(private dayService: DayService,
              private formBuilder: FormBuilder) {
    this.dayForm = this.formBuilder.group({
      day      : ['', [Validators.required, Validators.minLength(4)]],
      from     : [''],
      to     : ['']
    });
  }

  async save(dayForm) {
    const day = dayForm.value;
    day.id = this.day.id;

    this.dayService.updateDay(day)
        .subscribe(res => {
              this.closeDialog({type: 'edit', data: day});
            },
            err => {
              this.closeDialog({type: 'error', data: err});
            });
  }

  closeDialog(result) {
    // this.dayForm.reset();
    this.hide.emit(result);
  }

  ngOnChanges(change) {
    if (change.day) {
      if (change.day.currentValue) {
        this.dayForm.patchValue({
          day      : change.day.currentValue.day,
          from     : change.day.currentValue.from,
          to     : change.day.currentValue.to
        });
      }
    }
  }
}
