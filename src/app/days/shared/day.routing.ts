import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DayListComponent} from '../day-list/day-list.component';

const daysRoutes: Routes = [
  {
    path    : 'days',
    children: [
      {path: '', component: DayListComponent}
    ]
  }
];

export const DaysRouting: ModuleWithProviders = RouterModule.forChild(daysRoutes);
