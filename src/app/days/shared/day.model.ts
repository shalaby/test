export interface Day {
  id?: string;
  day: string;
  from?: string;
  to?: string;
}
